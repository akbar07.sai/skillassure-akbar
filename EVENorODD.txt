using System;
namespace Even
{
  public class Program  
   {  
     static void Main(string[] args)  
      {  
        //Step 1: Declaration of varialbles
        int num;
        Console.WriteLine("Enter a number:");
        num = int.Parse(Console.ReadLine());
        
        
        
        // Step 2: Calculation and displaying the result
        if(num % 2== 0)
        {
            Console.WriteLine("Given number is an even number");
        }
        else
          Console.WriteLine("Given number is a odd number");  
      }  
  }  
}